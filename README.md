https://github.com/nvim-lua/kickstart.nvim

Install Notes:
- `cord.nvim` is disabled by default, since it requires a rust compiler

Requirements
- `nvim-treesitter`: `gcc` or `clang` or any other `cc`
    - semantic parsing
        - syntax highlighting
        - jumps to object groups
- `cord`: `rustc` (you can install via rustup)
    - discord status

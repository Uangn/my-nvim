local function tokyobones()
  vim.g.tokyobones = {
    lightness = 'bright',
    transparent_background = true,
    --darken_comments = 45,
  }

  vim.cmd.colorscheme 'tokyobones'

  vim.cmd [[ set background=light ]]
  -- vim.cmd [[ set background=dark ]]

  vim.api.nvim_set_hl(0, 'ColorColumn', { bg = '#C7CBDB' })
end

return {
  {
    'zenbones-theme/zenbones.nvim',

    -- Optionally install Lush. Allows for more configuration or extending the colorscheme
    -- If you don't want to install lush, make sure to set g:zenbones_compat = 1
    -- In Vim, compat mode is turned on as Lush only works in Neovim.
    dependencies = 'rktjmp/lush.nvim',
    lazy = false,
    priority = 1000,

    -- you can set set configuration options here
    config = function()
      vim.g.neobones = {
        lightness = 'dim',
      }

      vim.g.zenbones = {
        lightness = 'dim',
      }

      tokyobones()
    end,
  },
}

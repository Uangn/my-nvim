return {
  {
    'pbrisbin/vim-colors-off',
    lazy = false,
    priority = 1000,

    init = function()
      vim.cmd [[ colorscheme off ]]
    end,
  },
}

return {
  {
    'RRethy/base16-nvim',

    config = function()
      vim.opt.termguicolors = true

      require('base16-colorscheme').setup {
        base00 = '#191313',
        base01 = '#2D2424',
        base02 = '#2D0B1C',
        base03 = '#9E695E',
        base04 = '#FF604F',
        base05 = '#FFEDE6',
        base06 = '#543F3F',
        base07 = '#BA9393',
        base08 = '#F95C90',
        base09 = '#F2A97E',
        base0A = '#FFE54F',
        base0B = '#4FFFA3',
        base0C = '#9AF6FF',
        base0D = '#7CCBFF',
        base0E = '#C255E0',
        base0F = '#9C6B61',
      }

      vim.api.nvim_set_hl(0, 'LspCodeLens', { fg = '#9E695E' })
    end,
  },
}

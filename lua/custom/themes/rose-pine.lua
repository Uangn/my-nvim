return {
  {
    'rose-pine/neovim',
    name = 'rose-pine',

    priority = 1000,
    init = function()
      vim.cmd.colorscheme 'rose-pine-dawn'
    end,

    opts = {
      variant = 'dawn', -- auto, main, moon, or dawn
      dark_variant = 'dawn', -- main, moon, or dawn
      dim_inactive_windows = false,
      extend_background_behind_borders = true,

      styles = {
        bold = false,
        italic = false,
        transparency = false,
      },
    },
  },
}

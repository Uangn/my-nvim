return {
  {
    'rebelot/kanagawa.nvim',
    priority = 1000,
    init = function()
      vim.cmd.colorscheme 'kanagawa'
    end,
    opts = {
      undercurl = true,

      commentStyle = { italic = false },
      functionStyle = {},
      keywordStyle = { italic = false },
      statementStyle = { bold = false },
      typeStyle = {},

      transparent = true,
      colors = {
        palette = {},
        theme = {
          all = {
            ui = {
              bg_gutter = 'none',
            },
          },
        },
      },

      background = {
        dark = 'dragon',
        light = 'lotus',
      },
    },
  },
}

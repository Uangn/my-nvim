return {
  -- See `:help gitsigns` to understand what the configuration keys do
  { -- Adds git related signs to the gutter, as well as utilities for managing changes
    'lewis6991/gitsigns.nvim',

    opts = {
      signs = {
        add = { text = '+' },
        change = { text = '~' },
        delete = { text = '_' },
        topdelete = { text = '‾' },
        changedelete = { text = '~' },
      },

      on_attach = function(bufnr)
        local gitsigns = require 'gitsigns'

        local function map(mode, l, r, opts)
          opts = opts or {}
          opts.buffer = bufnr
          vim.keymap.set(mode, l, r, opts)
        end

        -- Navigation
        map('n', ']h', function()
          if vim.wo.diff then
            vim.cmd.normal { ']h', bang = true }
          else
            gitsigns.nav_hunk 'next'
          end
        end, { desc = 'Git Hunk' })

        map('n', '[h', function()
          if vim.wo.diff then
            vim.cmd.normal { '[h', bang = true }
          else
            gitsigns.nav_hunk 'prev'
          end
        end, { desc = 'Git Hunk' })

        -- Actions
        map('n', '<leader>gs', gitsigns.stage_hunk, { desc = 'Git [S]tage Hunk' })
        map('v', '<leader>gs', function()
          gitsigns.stage_hunk { vim.fn.line '.', vim.fn.line 'v' }
        end, { desc = 'Git [S]tage Hunk' })
        map('n', '<leader>gu', gitsigns.undo_stage_hunk, { desc = 'Git [U]nStage Hunk' })
        -- map('n', '<leader>gr', gitsigns.reset_hunk, { desc = 'Git [R]eset Hunk' })
        -- map('v', '<leader>gr', function() gitsigns.reset_hunk {vim.fn.line('.'), vim.fn.line('v')} end
        --   , { desc = 'Git [R]eset Hunk' })

        map('n', '<leader>gp', gitsigns.preview_hunk, { desc = 'Git [P]review Hunk' })
        map('n', '<leader>gb', function()
          gitsigns.blame_line { full = true }
        end, { desc = 'Git [B]lame Line' })
        map('n', '<leader>gd', gitsigns.diffthis, { desc = 'Git [D]iff' })
        -- map('n', '<leader>gD', function() gitsigns.diffthis('~') end, { desc = 'Git [D]iff ~' })
        map('n', '<leader>tb', gitsigns.toggle_current_line_blame, { desc = 'Toggle Git [B]lame' })
        map('n', '<leader>td', gitsigns.toggle_deleted, { desc = 'Toggle Git [D]eleted' })

        -- Text object
        map({ 'o', 'x' }, 'ih', ':<C-U>Gitsigns select_hunk<CR>')
      end,
    },
  },
}

return {
  {
    'NeogitOrg/neogit',
    dependencies = {
      'nvim-lua/plenary.nvim',
      'sindrets/diffview.nvim',
      'nvim-telescope/telescope.nvim',
    },

    config = function()
      require('neogit').setup {
        -- disable_insert_on_commit = true,
        process_spinner = false,
      }

      require('which-key').add { { '<leader>g', name = '[G]it' } }
      require('which-key').add({
        { '<leader>g', name = '[G]it' },
      }, { mode = 'v' })
    end,
  },
}

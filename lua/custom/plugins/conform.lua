vim.api.nvim_create_user_command('ConformFormat', function()
  require('conform').format()
end, {
  desc = 'Formats Buffer',
})

return {
  { -- Autoformat
    'stevearc/conform.nvim',
    lazy = false,

    opts = {
      notify_on_error = false,

      format_on_save = function(bufnr)
        if vim.g.disable_autoformat or vim.b[bufnr].disable_autoformat then
          return
        end

        local disable_filetypes_on_save = {}
        if not disable_filetypes_on_save[vim.bo[bufnr].filetype] then
          return { timeout_ms = 500 }
        else
          return {}
        end
      end,

      formatters_by_ft = {
        c = { 'clang-format' },
        cpp = { 'clang-format' },
        clojure = { 'cljfmt' },
        haskell = { 'fourmolu', 'ormolu', stop_after_first = true },
        html = { 'prettier' },
        java = { 'google-java-format' },
        lua = { 'stylua' },
        purescript = { 'purescript-tidy' },
        python = function(bufnr)
          if require('conform').get_formatter_info('ruff_format', bufnr).available then
            return { 'ruff_format' }
          else
            return { 'isort', 'black' }
          end
        end,
        rust = { 'rustfmt' },
        cs = { 'csharpier' },
        typescript = { 'biome' },
        javascript = { 'biome' },
      },
    },

    config = function(_, opts)
      require('conform').setup(opts)

      local wkExists, wk = pcall(require, 'which-key')
      if wkExists then
        wk.add {
          {
            '<leader>cf',
            function()
              require('conform').format()
            end,
            mode = '',
            desc = 'Code [F]ormat buffer',
          },
        }
        wk.add({ { '<leader>c', name = '[C]ode' } }, { mode = 'v' })
      end

      vim.api.nvim_create_user_command('FormatDisable', function(args)
        if args.bang then
          -- FormatDisable! will disable formatting just for this buffer
          vim.b.disable_autoformat = true
          vim.notify 'Format on save Disabled (This buffer)'
        else
          vim.g.disable_autoformat = true
          vim.notify 'Format on save Disabled (Global)'
        end
      end, {
        desc = 'Disable autoformat-on-save',
        bang = true,
      })

      vim.api.nvim_create_user_command('FormatEnable', function()
        vim.b.disable_autoformat = false
        vim.g.disable_autoformat = false
        vim.notify 'Format on save Enabled'
      end, {
        desc = 'Re-enable autoformat-on-save',
      })

      vim.g.disable_autoformat = true
    end,
  },
}

return {
  {
    'NvChad/nvim-colorizer.lua',

    config = function()
      require('colorizer').setup {
        user_default_options = {
          names = false,
        },

        buftypes = {
          '*',
          -- exclude prompt and popup buftypes from highlight
          '!prompt',
          '!popup',
        },
      }
    end,
  },
}

return {
  {
    'jiaoshijie/undotree',
    dependencies = 'nvim-lua/plenary.nvim',
    opts = {},
    keys = {
      { '<leader>cu', "<cmd>lua require('undotree').toggle()<cr>", desc = 'Code [U]ndotree' },
    },
  },
}

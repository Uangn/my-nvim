return {
  -- https://github.com/Aloxaf/silicon
  {
    'michaelrommel/nvim-silicon',
    lazy = true,
    cmd = 'Silicon',

    opts = {
      font = 'FiraCode-Regular Font=34;Noto Color Emoji',
      theme = 'Visual Studio Dark+',
      background = '#55617A',
      pad_horiz = 60,
      pad_vert = 40,
      no_round_corner = false,
      no_window_controls = true,
      no_line_number = false,
      num_separator = '\u{258f} ',
      line_offset = function(args)
        return args.line1
      end,
      line_pad = 0,
      tab_width = 4,
      -- language = function()
      --   return vim.bo.filetype
      -- end,
      -- if the shadow below the os window should have be blurred
      shadow_blur_radius = 16,
      -- the offset of the shadow in x and y directions
      shadow_offset_x = 8,
      shadow_offset_y = 8,
      -- the color of the shadow
      shadow_color = '#100808',

      -- whether to put the image onto the clipboard, may produce an error if run on WSL2
      to_clipboard = true,
      command = 'silicon',
      -- output = function()
      --   return './' .. os.date '!%Y-%m-%dT%H-%M-%S' .. '_code.png'
      -- end,

      window_title = nil,
    },
  },
}

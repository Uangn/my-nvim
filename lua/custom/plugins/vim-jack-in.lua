return {
  {
    'clojure-vim/vim-jack-in',
    dependencies = {
      'tpope/vim-dispatch',
      'radenling/vim-dispatch-neovim',
    },

    config = function()
      vim.cmd [[command! -bang -nargs=* Clj call jack_in#clj(<bang>1,<q-args>)]]
    end,
  },
}

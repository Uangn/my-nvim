return {
  {
    'mrcjkb/haskell-tools.nvim',
    version = '^4',
    lazy = false,
    ft = { 'haskell', 'lhaskell', 'cabal', 'cabalproject' },

    config = function()
      vim.g.haskell_tools = {
        tools = {
          repl = {
            auto_focus = true,
          },

          hover = {
            enable = false,
          },
        },

        hls = {
          settings = {
            haskell = {
              plugin = {
                -- -- missing class methods
                -- class = { codeLensOn = false, },
                -- -- make import lists fully explicit
                -- importLens = { codeLensOn = false, },
                -- -- refine imports
                -- refineImports = { codeLensOn = false, },
                -- -- wingman
                -- tactics = { codeLensOn = false, },
                -- -- fix module names
                -- moduleName = { globalOn = false, },
                -- -- evaluate code snippets
                -- eval = { globalOn = false, },
                -- -- show/add missing type signatures
                -- ['ghcide-type-lenses'] = { globalOn = false, },
              },
            },
          },
        },
      }

      local ht = require 'haskell-tools'

      vim.api.nvim_create_autocmd({ 'BufEnter' }, {
        pattern = { '*.hs' },
        group = vim.api.nvim_create_augroup('my-haskell-tools', { clear = true }),
        callback = function(event)
          local bufnr = event.buf

          -- haskell-language-server relies heavily on codeLenses,
          -- so auto-refresh (see advanced configuration) is enabled by default
          vim.keymap.set('n', '<localleader>c', vim.lsp.codelens.run, { noremap = true, silent = true, buffer = bufnr, desc = '[C]odeLens (Haskell)' })

          -- Hoogle search for the type signature of the definition under the cursor
          vim.keymap.set('n', '<localleader>s', ht.hoogle.hoogle_signature, { noremap = true, silent = true, buffer = bufnr, desc = '[S]earch (Haskell)' })

          -- Evaluate all code snippets
          vim.keymap.set('n', '<localleader>e', ht.lsp.buf_eval_all, { noremap = true, silent = true, buffer = bufnr, desc = '[E]valuate Code (Haskell)' })

          require('which-key').add { { '<localleader>r', name = '[R]epl' } }

          -- Toggle a GHCi repl for the current package
          vim.keymap.set('n', '<localleader>rp', ht.repl.toggle, { noremap = true, silent = true, buffer = bufnr, desc = 'Repl [P]ackage (Haskell)' })

          -- Toggle a GHCi repl for the current buffer
          vim.keymap.set('n', '<localleader>rf', function()
            ht.repl.toggle(vim.api.nvim_buf_get_name(0))
          end, { noremap = true, silent = true, buffer = bufnr, desc = 'Repl [F]ile (Haskell)' })
          vim.keymap.set('n', '<localleader>rq', ht.repl.quit, { noremap = true, silent = true, buffer = bufnr, desc = 'Repl [Q]uit (Haskell)' })
        end,
      })
    end,
  },
}

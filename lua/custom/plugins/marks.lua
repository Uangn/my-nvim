return {
  {
    'chentoast/marks.nvim',
    event = 'VeryLazy',

    opts = {
      -- whether to map keybinds or not. default true
      default_mappings = true,

      mappings = {},
    },
  },
}

return {
  -- NOTE: Highlight todo, notes, etc in comments
  {
    'folke/todo-comments.nvim',
    event = 'VimEnter',
    dependencies = { 'nvim-lua/plenary.nvim' },

    opts = {
      signs = false,

      search = {
        pattern = [[\b(KEYWORDS)(\([^\)]*\))?:]],
      },

      highlight = {
        before = '',
        keyword = 'bg',
        after = '',
        pattern = [[.*<((KEYWORDS)%(\(.{-1,}\))?):]],
      },
    },
  },
}

return {
  {
    'shortcuts/no-neck-pain.nvim',
    version = '*',
    enabled = false,

    opts = {
      autocmds = {
        enableOnVimEnter = true,
      },

      buffers = {
        right = {
          enabled = false,
        },
      },
    },

    config = function(_, opts)
      require('no-neck-pain').setup(opts)
    end,
  },
}

return {
  { -- Useful plugin to show you pending keybinds.
    'folke/which-key.nvim',

    event = 'VimEnter', -- Sets the loading event to 'VimEnter'

    config = function() -- This is the function that runs, AFTER loading
      require('which-key').setup {
        delay = function(ctx)
          return ctx.plugin and 0 or 1000
        end,
        win = {
          no_overlap = false,
        },
        icons = {
          mappings = false,
        },
        keys = {
          scroll_down = '<c-j>',
          scroll_up = '<c-k>',
        },
      }
    end,
  },
}

return {
  'fedepujol/move.nvim',

  opts = {
    line = {
      indent = false,
    },
    block = {
      indent = false,
    },
    char = {
      -- enable = true,
    },
  },

  config = function(_, opts)
    require('move').setup(opts)
  end,
}

return {
  {
    'folke/trouble.nvim',

    init = function()
      require('which-key').add { { '<leader>q', name = '[Q]uickfix' } }
    end,

    keys = {
      {
        '<leader>qd',
        '<cmd>Trouble diagnostics toggle<cr>',
        desc = 'Diagnostics (Trouble)',
      },
      {
        '<leader>qt',
        '<cmd>Trouble todo toggle<cr>',
        desc = 'Todo (Trouble)',
      },
      {
        '<leader>qs',
        '<cmd>Trouble symbols toggle<cr>',
        desc = 'Symbols (Trouble)',
      },
      {
        '<leader>ql',
        '<cmd>Trouble lsp toggle<cr>',
        desc = 'LSP Definitions / references / ... (Trouble)',
      },
      {
        '<leader>qL',
        '<cmd>Trouble loclist toggle<cr>',
        desc = 'Location List (Trouble)',
      },
      {
        '<leader>qq',
        '<cmd>Trouble qflist toggle<cr>',
        desc = 'Quickfix List (Trouble)',
      },
    },

    opts = {},
  },
}

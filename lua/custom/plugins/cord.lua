local blacklist_list = {
}

local is_blacklisted = function(opts)
  return (vim.tbl_contains(blacklist_list, opts.workspace))
end

local get_errors = function(bufnr)
  return vim.diagnostic.get(bufnr, { severity = vim.diagnostic.severity.ERROR })
end
local errors = get_errors(0) -- pass the current buffer; pass nil to get errors for all buffers

vim.api.nvim_create_autocmd('DiagnosticChanged', {
  callback = function()
    errors = get_errors(0)
  end,
})

return {
  {
    'vyfor/cord.nvim',
    build = '<cmd>Cord update',

    opts = {
      enabled = true,
      log_level = vim.log.levels.OFF,

      editor = {
        tooltip = 'not emacs',
      },

      display = {
        theme = 'pastel',
      },

      text = {
        workspace = function(opts)
          return opts.repo_url and ('In ' .. opts.workspace) or 'Working hard on the next feature :)'
        end,
        viewing = function(opts)
          return 'Viewing ' .. opts.filename
        end,
        editing = function(opts)
          local text = string.format('Editing %s - %s:%s - %s errors', (opts.repo_url and opts.filename or 'a-secret'), opts.cursor_line, opts.cursor_char, #errors)
          if vim.bo.modified then text = text .. ' [+]' end
          return text
        end,
      },

      idle = {
        details = function(opts)
          return string.format('Taking a break from %s', opts.workspace)
        end,
      },

      hooks = {
        workspace_change = function(opts)
          if is_blacklisted(opts) then
            opts.manager:hide()
          else
            opts.manager:resume()
          end
        end,
      },
    },
  },
}

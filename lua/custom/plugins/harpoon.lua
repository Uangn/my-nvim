return {
  {
    'ThePrimeagen/harpoon',
    branch = 'harpoon2',
    dependencies = { 'nvim-lua/plenary.nvim' },

    init = function()
      require('which-key').add { { '<leader>h', name = '[H]arpoon' } }
    end,

    config = function()
      local harpoon = require 'harpoon'

      harpoon:setup()

      -- Mark
      local mark = function()
        require('harpoon'):list():add()
      end
      local mark_opts = { desc = 'Harpoon [M]ark' }
      vim.keymap.set('n', '<M-m>', mark, mark_opts)
      vim.keymap.set('n', '<leader>hm', mark, mark_opts)

      -- List
      local list = function()
        require('harpoon').ui:toggle_quick_menu(require('harpoon'):list())
      end
      local list_opts = { desc = 'Harpoon [L]ist' }
      vim.keymap.set('n', '<M-l>', list, list_opts)
      vim.keymap.set('n', '<leader>hl', list, list_opts)

      -- Navigation
      local select1 = function()
        require('harpoon'):list():select(1)
      end
      local select1_opts = { desc = 'Harpoon 1 [F]' }
      vim.keymap.set('n', '<leader>hf', select1, select1_opts)

      local select2 = function()
        require('harpoon'):list():select(2)
      end
      local select2_opts = { desc = 'Harpoon 2 [D]' }
      vim.keymap.set('n', '<leader>hd', select2, select2_opts)

      local select3 = function()
        require('harpoon'):list():select(3)
      end
      local select3_opts = { desc = 'Harpoon 3 [S]' }
      vim.keymap.set('n', '<leader>hs', select3, select3_opts)

      local select4 = function()
        require('harpoon'):list():select(4)
      end
      local select4_opts = { desc = 'Harpoon 4 [A]' }
      vim.keymap.set('n', '<leader>ha', select4, select4_opts)

      -- Toggle previous & next buffers stored within Harpoon list
      local prev = function()
        require('harpoon'):list():prev()
      end
      local prev_opts = { desc = 'Harpoon [P]rev' }
      vim.keymap.set('n', '<leader>hp', prev, prev_opts)

      local next = function()
        require('harpoon'):list():next()
      end
      local next_opts = { desc = 'Harpoon [N]ext' }
      vim.keymap.set('n', '<leader>hn', next, next_opts)
    end,
  },
}

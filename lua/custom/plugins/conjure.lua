return {
  {
    'Olical/conjure',

    init = function()
      -- vim.g['conjure#log#hud#enabled'] = false
      -- vim.g['conjure#log#hud#border'] = 'none'
      vim.g['conjure#mapping#eval_visual'] = 'e'
      vim.g['conjure#mapping#doc_word'] = 'k'

      local conjure_augroup = vim.api.nvim_create_augroup('conjure', { clear = true })
      vim.api.nvim_create_autocmd({ 'BufEnter' }, {
        group = conjure_augroup,
        callback = function()
          if vim.fn.index(vim.g['conjure#filetypes'], vim.bo.filetype) >= 0 then
            require('which-key').add {
              { '<localleader>c', name = '[C]onnect' },
              { '<localleader>e', name = '[E]valuate' },
              { '<localleader>g', name = '[G]o To' },
              { '<localleader>l', name = '[L]og' },
              { '<localleader>r', name = '[R]efresh' },
              { '<localleader>s', name = '[S]ession' },
              { '<localleader>t', name = '[T]est' },
              { '<localleader>v', name = '[V]iew' },
            }
          end
        end,
      })
    end,
  },
}

return {
  {
    'stevearc/oil.nvim',

    opts = {
      columns = {
        -- 'permissions',
        -- 'size',
        'mtime',
        'icon',
      },

      natural_order = true,

      delete_to_trash = true,

      -- -- EXPERIMENTAL support for performing file operations with git
      -- git = {
      --   -- Return true to automatically git add/mv/rm files
      --   add = function(path)
      --     return false
      --   end,
      --   mv = function(src_path, dest_path)
      --     return false
      --   end,
      --   rm = function(path)
      --     return false
      --   end,
      -- },

      keymaps = {
        ['g?'] = 'actions.show_help',
        ['<CR>'] = 'actions.select',
        ['L'] = 'actions.select',
        ['<C-s>'] = 'actions.select_vsplit',
        ['<C-h>'] = 'actions.select_split',
        ['<C-t>'] = 'actions.select_tab',
        ['<C-p>'] = 'actions.preview',
        ['<C-c>'] = 'actions.close',
        ['<C-l>'] = 'actions.refresh',
        ['-'] = 'actions.parent',
        ['H'] = 'actions.parent',
        ['_'] = 'actions.open_cwd',
        ['`'] = 'actions.cd',
        ['~'] = 'actions.tcd',
        ['gs'] = 'actions.change_sort',
        ['gx'] = 'actions.open_external',
        ['g.'] = 'actions.toggle_hidden',
        ['g\\'] = 'actions.toggle_trash',
      },

      view_options = {
        show_hidden = true,
      },
    },

    config = function(_, opts)
      require('oil').setup(opts)

      vim.keymap.set('n', '<leader>uf', '<cmd>Oil<cr>', { desc = 'UI [F]ile Explorer' })
    end,

    dependencies = { 'nvim-tree/nvim-web-devicons' },
  },
}

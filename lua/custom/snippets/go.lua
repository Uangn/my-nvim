return {
  s(
    'iferr',
    fmt(
      [[
        if err != nil {
            return <>, err
        }
      ]],
      { i(0) },
      { delimiters = '<>' }
    )
  ),
}, {}

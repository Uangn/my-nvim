local M = {}

-- Utils
local make_input__save_prev_submit = ''

local function make_select(tbl, f, opts)
  vim.ui.select(tbl, {
    format_item = function(e)
      return e.message
    end,
  }, function(e, i)
    if opts.save_prev_submit then
      make_input__save_prev_submit = e.result
    end
    f(e)
  end)
end
M.make_select = make_select

local function make_input(topText, onSubmit, opts)
  return function()
    local Input = require 'nui.input'
    local event = require('nui.utils.autocmd').event

    local input = Input({
      relative = 'editor',
      position = '50%',
      size = {
        width = 40,
        height = 20,
      },
      border = {
        padding = { 0, 0 },
        style = 'single',
        text = {
          top = '[' .. topText .. ']',
          top_align = 'center',
        },
      },
      win_options = {
        winhighlight = 'Normal:Normal,FloatBorder:Normal',
      },
    }, {
      prompt = '> ',
      default_value = make_input__save_prev_submit,
      on_close = function() end,
      on_submit = function(s)
        if opts.save_prev_submit then
          make_input__save_prev_submit = s
        end
        return onSubmit(s)
      end,
    })

    -- mount/open the component
    input:mount()
    vim.keymap.set('n', 'q', function()
      input:unmount()
    end, { noremap = true })
    vim.keymap.set('n', '<esc>', function()
      input:unmount()
    end, { noremap = true })

    -- unmount component when cursor leaves buffer
    input:on(event.BufLeave, function()
      input:unmount()
    end)
  end
end
M.make_input = make_input

local function calm_error(f, ...)
  local ok, r = pcall(f, ...)
  if not ok then
    print(r)
  end
end
M.calm_error = calm_error

local function escape_string(x)
  return string.gsub(x, ' ', '\\ ')
end
M.escape_string = escape_string

return M

-- [[ Basic Keymaps ]]
--  See `:help vim.keymap.set()`

local W = {}

local helpers = require 'settings.keymap-helpers'

-- QOL Quick Edits
-- Newlines
vim.keymap.set('n', '<c-enter>', 'i<enter><esc>')
vim.keymap.set('n', '<c-s-enter>', '<s-o><esc>')

---- Move
local o = { noremap = true, silent = true }
-- Normal-mode commands
vim.keymap.set('n', '<C-A-j>', ':MoveLine(1)<CR>', o)
vim.keymap.set('n', '<C-A-k>', ':MoveLine(-1)<CR>', o)
vim.keymap.set('n', '<C-A-h>', ':MoveHChar(-1)<CR>', o)
vim.keymap.set('n', '<C-A-l>', ':MoveHChar(1)<CR>', o)

-- Visual-mode commands
vim.keymap.set('v', '<C-A-j>', ':MoveBlock(1)<CR>', o)
vim.keymap.set('v', '<C-A-k>', ':MoveBlock(-1)<CR>', o)
vim.keymap.set('v', '<C-A-h>', ':MoveHBlock(-1)<CR>', o)
vim.keymap.set('v', '<C-A-l>', ':MoveHBlock(1)<CR>', o)

-- Terminal Binds
vim.keymap.set('t', '<C-w>', '<c-\\><c-n>')
vim.keymap.set('n', '<leader>tt', '<cmd>FloatTerminalToggle<cr>', { desc = 'Toggle [T]erminal' })

-- Vim DadBod
vim.keymap.set('n', '<leader>ud', '<cmd>DBUI<cr>', { desc = 'UI [D]atabase' })

-- Colortils
vim.keymap.set('n', '<leader>uh', '<cmd>Colortils<cr>', { desc = 'UI [H]ex Color Utils' })

-- Git
vim.keymap.set('n', '<leader>gh', '<cmd>Neogit cwd=%:p:h<cr>', { desc = 'Git [H]ere' })
vim.keymap.set('n', '<leader>gg', '<cmd>Neogit<cr>', { desc = 'Git [G]it' })

-- Set highlight on search, but clear on pressing <Esc> in normal mode
vim.opt.hlsearch = true
vim.keymap.set('n', '<esc>', '<cmd>nohlsearch<cr><esc>')

-- Diagnostic keymaps
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, { desc = '[E]rror messages' })
vim.keymap.set('n', '<leader>uq', '<cmd>copen<cr>', { desc = 'UI [Q]uickfix list' })
vim.keymap.set('n', '<leader>ul', '<cmd>lopen<cr>', { desc = 'UI [L]ocation list' })

-- Code keymaps
W[#W + 1] = { '<leader>c', name = '[C]ode' }
vim.keymap.set('n', '<leader>cw', '<cmd>cd %:p:h<cr>', { desc = 'CD to [W]orking Directory' })
vim.keymap.set('n', '<leader>cm', '<cmd>Man<cr>', { desc = 'Code [M]an' })

-- Lsp keymaps
W[#W + 1] = { '<leader>cl', name = 'Code [L]SP' }
vim.keymap.set('n', '<leader>clr', '<cmd>LspRestart<cr>', { desc = 'Code [L]sp [R]estart' })
vim.keymap.set('n', '<leader>cll', '<cmd>LspLog<cr>', { desc = 'Code [L]sp [L]og' })
vim.keymap.set('n', '<leader>cli', '<cmd>LspInfo<cr>', { desc = 'Code [L]sp [I]nfo' })
vim.keymap.set('n', '<leader>cls', '<cmd>LspStart<cr>', { desc = 'Code [L]sp [S]tart' })
vim.keymap.set('n', '<leader>cle', '<cmd>LspStop<cr>', { desc = 'Code [L]sp [E]nd' })

-- Make Prg
W[#W + 1] = { '<leader>m', name = '[M]ake' }

vim.keymap.set('n', '<leader>mm', '<cmd>make<cr>', { desc = '[M]ake Run' })

vim.keymap.set(
  'n',
  '<leader>mc',
  helpers.make_input('MakePrg', function(v)
    helpers.calm_error(vim.cmd, 'set makeprg=' .. helpers.escape_string(v))
  end, { save_prev_submit = true }),
  { desc = 'Make [C]reate Command' }
)

vim.keymap.set('n', '<leader>ms', function()
  helpers.make_select({
    { message = 'haskell: stack', result = 'stack build' },
    { message = 'haskell: cabal', result = 'cabal build' },
    { message = 'c: make', result = 'make' },
    { message = 'rust: cargo', result = 'cargo build' },
  }, function(e)
    vim.cmd('set makeprg=' .. helpers.escape_string(e.result))
  end, { save_prev_submit = true })
end, { desc = 'Make [S]elect Command' })

-- UI
W[#W + 1] = { '<leader>u', name = '[U]I' }
vim.keymap.set('n', '<leader>up', '<cmd>Lazy<cr>', { desc = 'UI [P]ackage Manager' })
vim.keymap.set('n', '<leader>ut', '<cmd>Mason<cr>', { desc = 'UI [T]ooling Manager' })
vim.keymap.set('n', '<leader>um', '<cmd>messages<cr>', { desc = 'UI [M]essages' })

-- Disable arrow keys in normal mode
vim.keymap.set('n', '<left>', '<cmd>echo "Use h to move!!"<CR>')
vim.keymap.set('n', '<right>', '<cmd>echo "Use l to move!!"<CR>')
vim.keymap.set('n', '<up>', '<cmd>echo "Use k to move!!"<CR>')
vim.keymap.set('n', '<down>', '<cmd>echo "Use j to move!!"<CR>')

-- Navigation
---- Center Screen When Scrolling
vim.keymap.set('n', '<C-d>', '<C-d>zz')
vim.keymap.set('n', '<C-u>', '<C-u>zz')

---- Navigate Wrapped Lines
-- vim.keymap.set('n', 'j', 'gj')
-- vim.keymap.set('n', 'k', 'gk')

-- Yanks
vim.keymap.set({ 'n', 'v' }, '<leader>y', [["+y]], { desc = '[Y]ank System Clipboard' })
vim.keymap.set('n', '<leader>Y', [["+Y]], { desc = '[Y]ank Line System Clipboard' })

-- Toggle
W[#W + 1] = { '<leader>t', name = '[T]oggle' }

---- Colorizer
vim.keymap.set('n', '<leader>th', '<cmd>ColorizerToggle<cr>', { desc = 'Toggle [H]ex Colors' })

---- No Neck Pain
vim.keymap.set('n', '<leader>tn', '<cmd>NoNeckPain<cr>', { desc = 'Toggle [N]o Neck Pain' })

---- cmp
local function toggle_cmp()
  local cmp = require 'cmp'
  local current_setting = cmp.get_config().completion.autocomplete
  if current_setting and #current_setting > 0 then
    cmp.setup { completion = { autocomplete = false } }
    vim.notify 'Autocomplete disabled'
  else
    cmp.setup { completion = { autocomplete = { cmp.TriggerEvent.TextChanged } } }
    vim.notify 'Autocomplete enabled'
  end
end
vim.keymap.set('n', '<leader>tc', toggle_cmp, { desc = 'Toggle [C]mp' })

---- Format
local function toggle_formatOnSave()
  if vim.g.disable_autoformat then
    vim.cmd [[FormatEnable]]
  else
    vim.cmd [[FormatDisable]]
  end
end
vim.keymap.set('n', '<leader>tf', toggle_formatOnSave, { desc = 'Toggle [F]ormat On Save' })

---- Virtual Text
vim.virtualOn = false
local function toggle_virtual()
  vim.virtualOn = not vim.virtualOn
  vim.diagnostic.config { virtual_text = vim.virtualOn }
  if vim.virtualOn then
    vim.notify 'Virtual Text Enabled'
  else
    vim.notify 'Virtual Text Disabled'
  end
end
vim.diagnostic.config { virtual_text = vim.virtualOn }
vim.keymap.set('n', '<leader>tv', toggle_virtual, { desc = 'Toggle [V]irtual Text' })

---- Line Wrap
local function toggle_wrap()
  vim.o.wrap = not vim.o.wrap
  if vim.o.wrap then
    vim.notify 'Line Wrapping Enabled'
  else
    vim.notify 'Line Wrapping Disabled'
  end
end
vim.keymap.set('n', '<leader>tw', toggle_wrap, { desc = 'Toggle Line [W]rapping' })

-- Panes
W[#W + 1] = { '<leader>p', name = '[P]ane / Buffer' }
vim.keymap.set('n', '<leader>pq', '<cmd>q<cr>', { desc = 'Pane [Q]uit' })
vim.keymap.set('n', '<leader>pc', '<cmd>close<cr>', { desc = 'Pane [C]lose Buffer' })
vim.keymap.set('n', '<leader>pd', '<cmd>bd<cr>', { desc = 'Pane [D]elete Buffer' })

vim.keymap.set('n', '<leader>ps', '<cmd>bo term<cr>', { desc = 'Pane [S]hell' })

vim.keymap.set('n', '<leader>v', '<cmd>vsp<cr>', { desc = '[V]ertical Split' })
vim.keymap.set('n', '<leader>pv', '<cmd>vsp<cr>', { desc = 'Pane [V]ertical Split' })
vim.keymap.set('n', '<leader>x', '<cmd>sp<cr>', { desc = '[X] Horizontal Split' })
vim.keymap.set('n', '<leader>px', '<cmd>sp<cr>', { desc = 'Pane [X] Horizontal Split' })
vim.keymap.set('n', '<leader>pt', '<cmd>tab split<cr>', { desc = 'Pane [T]ab New' })

vim.keymap.set('n', '<leader>p|', '<cmd>vertical resize<cr>', { desc = '[|] Pane Maximise Vertically' })
vim.keymap.set('n', '<leader>p_', '<cmd>horizontal resize<cr>', { desc = '[_] Pane Maximise Horizontally' })
vim.keymap.set('n', '<leader>pz', '<cmd>horizontal resize<cr><cmd>vertical resize<cr>', { desc = '[Z] Pane Maximise' })
vim.keymap.set('n', '<leader>pZ', '<cmd>wincmd =<cr>', { desc = '[Z] Pane Equalize Sizes' })

return W
